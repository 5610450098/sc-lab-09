package tree_traversal;

public class Node {
	private String node;
	private Node left;
	private Node right;
	
	public Node(String node){
		this(node,null,null);
	}
	public Node(String node,Node left,Node right){
		this.node = node;
		this.left = left;
		this.right = right;
	}
	public String getNode(){
		return node;
	}
	public Node getLeft(){
		return left;
	}
	public Node getRight(){
		return right;
	}
}
