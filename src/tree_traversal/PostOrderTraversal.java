package tree_traversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal {
	@Override
	public List<Node> traverse(Node node){
		List<Node> tree;
		if(node==null){
			return new ArrayList<Node>();
		}
		tree = traverse(node.getLeft());
		tree.addAll(traverse(node.getRight()));
		tree.add(node);
		return tree;		
	}

}
