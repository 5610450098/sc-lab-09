package tree_traversal;

import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal {
	@Override
	public List<Node> traverse(Node node){
		List<Node> tree;
		if(node==null){
			return new ArrayList<Node>();
		}
		tree = traverse(node.getLeft());
		tree.add(node);
		tree.addAll(traverse(node.getRight()));
		return tree;		
	}

}
