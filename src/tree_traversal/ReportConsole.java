package tree_traversal;

import java.util.List;

public class ReportConsole {
	public void display(Node root,Traversal traversal){
		List<Node> node = traversal.traverse(root);
		String name = traversal.getClass().getSimpleName();
		System.out.print("Traverse with "+name+" : ");
		for(Node n : node){
			System.out.print(n.getNode()+" ");
		}
		System.out.println();
	}
}
