package testcase;

import tree_traversal.InOrderTraversal;
import tree_traversal.Node;
import tree_traversal.PostOrderTraversal;
import tree_traversal.PreOrderTraversal;
import tree_traversal.ReportConsole;

public class TestTraverse {
	private ReportConsole rep;
	public static void main(String[] args){		
		new TestTraverse();
	}
	public TestTraverse(){
		testCase();
	}
	public void testCase(){
		Node a = new Node("A",null,null);
		Node c = new Node("C",null,null);
		Node e = new Node("E",null,null);
		Node d = new Node("D",c,e);
		Node b = new Node("B",a,d);
		Node h = new Node("H",null,null);
		Node i = new Node("I",h,null);
		Node g = new Node("G",null,i);
		Node f = new Node("F",b,g);

		rep = new ReportConsole();
		rep.display(f, new PreOrderTraversal());
		rep.display(f, new InOrderTraversal());
		rep.display(f, new PostOrderTraversal());
	}
}
