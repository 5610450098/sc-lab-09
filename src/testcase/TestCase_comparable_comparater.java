package testcase;
import java.util.ArrayList;
import java.util.Collections;

import Comparable_Comparater.AllComparator;
import Comparable_Comparater.Company;
import Comparable_Comparater.EarningComparator;
import Comparable_Comparater.ExpenseComparator;
import Comparable_Comparater.Person;
import Comparable_Comparater.Product;
import Comparable_Comparater.ProfitComparator;
import Comparable_Comparater.Taxable;



public class TestCase_comparable_comparater {
	public static void main(String[] args){
		new TestCase_comparable_comparater();
	}
	public TestCase_comparable_comparater(){
		testPerson();
		testProduct();
		testCompany();
		testAllTax();
	}
	public void testPerson(){
		ArrayList<Person> person = new ArrayList<Person>();
		person.add(new Person("Black",500000));
		person.add(new Person("Wut",300200));
		person.add(new Person("Pond",105000));
		System.out.println("Test Person salary/year sort");
		System.out.println("--before sort salary in person--");
		for(Person p : person){
			System.out.println(p);
		}
		Collections.sort(person);
		System.out.println("--after sort salary in person--");
		for(Person p : person){
			System.out.println(p);
		}
	}
	
	public void testProduct(){
		ArrayList<Product> product = new ArrayList<Product>();
		product.add(new Product("Computer",15300));
		product.add(new Product("Choco",55));
		product.add(new Product("Book",350));
		System.out.println("\nTest Product price sort");
		System.out.println("--before sort price in product--");
		for(Product p : product){
			System.out.println(p);
		}
		Collections.sort(product);
		System.out.println("--after sort price in product--");
		for(Product p : product){
			System.out.println(p);
		}
	}
	public void testCompany(){
		ArrayList<Company> company = new ArrayList<Company>();
		company.add(new Company("Boom",5400,6200));
		company.add(new Company("Mark",8500,5400));
		company.add(new Company("June",4500,3000));
		System.out.println("\nTest company income sort");
		System.out.println("--before sort in company--");
		for(Company c : company){
			System.out.println(c);
		}
		Collections.sort(company,new EarningComparator());
		System.out.println("--after sort income in company--");
		for(Company c : company){
			System.out.println(c);
		}
		Collections.sort(company,new ExpenseComparator());
		System.out.println("--after sort expense in company--");
		for(Company c : company){
			System.out.println(c);
		}
		Collections.sort(company,new ProfitComparator());
		System.out.println("--after sort profit in company--");
		for(Company c : company){
			System.out.println(c);
		}
	}
	public void testAllTax(){
		ArrayList<Taxable> tax = new ArrayList<Taxable>();
		tax.add(new Person("Wut",300200));
		tax.add(new Person("Pond",105000));
		tax.add(new Company("Boom",15000,6200));
		tax.add(new Company("Mark",55500,9000));
		tax.add(new Product("Computer",15300));
		tax.add(new Product("Iphone",42000));
		System.out.println("\nTest all tax");
		System.out.println("--before sort tax--");
		for(Taxable t : tax){
			System.out.println(t+"\ttax: "+t.getTax());
		}
		System.out.println("--after sort tax--");
		Collections.sort(tax,new AllComparator());
		for(Taxable t : tax){
			System.out.println(t+"\ttax: "+t.getTax());
		}
	}
}
