package Comparable_Comparater;


public class Person implements Taxable,Comparable<Person>{
	private String name;
	private double salary;
	public Person(String name,double salary){
		this.name = name;
		this.salary = salary;
	}
	
	public String getName(){
		return this.name;
	}
	public double getSalary(){
		return this.salary;
	}
	
	@Override
	public double getTax() {
		if(this.getSalary()<=300000){
			return this.getSalary()*0.05;
		}
		else{
			return (300000*0.05)+((this.getSalary()-300000)*0.1);
		}
	}

	@Override
	public int compareTo(Person other) {
		if(this.salary<other.salary){return -1;}
		if(this.salary>other.salary){return 1;}
		return 0;
	}
	
	public String toString(){
		return "name : "+this.name+"\tsalary : "+this.salary+" bath/year";
	}


}
