package Comparable_Comparater;
import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company> {
	@Override
	public int compare(Company c1,Company c2){
		if(c1.getExpense()<c2.getExpense()){return -1;}
		if(c1.getExpense()>c2.getExpense()){return 1;}
		return 0;
	}
}
