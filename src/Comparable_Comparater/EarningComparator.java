package Comparable_Comparater;
import java.util.Comparator;


public class EarningComparator implements Comparator<Company>{

	@Override
	public int compare(Company c1, Company c2) {
		if(c1.getIncome()<c2.getIncome()){return -1;}
		if(c1.getIncome()>c2.getIncome()){return 1;}
		return 0;
	}
	
}
