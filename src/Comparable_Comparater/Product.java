package Comparable_Comparater;


public class Product implements Taxable,Comparable<Product>{
	private String name;
	private double price;
	
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}
	public String getName(){
		return this.name;
	}
	public double getPrice(){
		return this.price;
	}
	
	@Override
	public double getTax() {
		return this.getPrice()*7/100;
	} 
	
	@Override
	public int compareTo(Product other){
		if(this.price<other.price){return -1;}
		if(this.price>other.price){return 1;}
		return 0;
	}
	
	public String toString(){
		return "name : "+this.name+"\tprice : "+this.price+" bath";
	}

}
