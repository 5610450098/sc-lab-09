package Comparable_Comparater;
import java.util.Comparator;


public class ProfitComparator implements Comparator<Company> {
	@Override
	public int compare(Company c1,Company c2){
		if(c1.getProfit()<c2.getProfit()){return -1;}
		if(c1.getProfit()>c2.getProfit()){return 1;}
		return 0;
	}

}
