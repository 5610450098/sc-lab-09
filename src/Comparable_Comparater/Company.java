package Comparable_Comparater;


public class Company implements Taxable{
	private String name;
	private double income;
	private double expense;
	
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}
	
	public String getName(){
		return this.name;
	}
	public double getIncome(){
		return this.income;
	}
	public double getExpense(){
		return this.expense;
	}
	public double getProfit(){
		return this.income-this.expense;
	}
	@Override
	public double getTax() {
		if(this.getExpense()>this.getIncome()){
			return 0;
		}
		else{
			return (this.getIncome()-this.getExpense())*0.3;
		}
	}
	
	public String toString(){
		return "name : "+this.name+"\tincome: "+this.income+"\texpense: "+this.expense+"\tprofit: "+this.getProfit();
	}

}
